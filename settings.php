<?php

/**
 * @package     Joomla.Site
 * @subpackage  Templates.ms
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

// Подключаем настройки
//require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/settings.php');
//$knS = new KnSettings();

// Новая версия подключения
//require_once (JPATH_ROOT.'/templates/ms/settings.php');
//$knS = new KnSettings();
class KnSettings
{
	// kH1 mod_menu
	const MOD_MENU_ItemClass = 0; // Классы для каждого пункта меню которые указываются в li
	const MOD_MENU_Anchor_css = 1; // Классы из CMS
	const MOD_MENU_FirstItem = 1; // Отключаем дублирование главного пункта меню

	// kH1 burger_menu
	const BURGER_MENU_ItemClass = 0; // Классы для каждого пункта меню которые указываются в li
	const BURGER_MENU_Anchor_css = 1; // Классы из CMS
	const BURGER_MENU_FirstItem = 1; // Отключаем дублирование главного пункта меню

	//kH1 Template
	const BURGER_LogoCircle = 0; // Отображение кружка перед Логотипом
	const MAIN_MENU_LogoCircle = 0;
	const BURGER_LogoImg = 'templates/ms/img/favicon/android-chrome-512x512.png'; // Изображение перед логотипом
	const MAIN_MENU_LogoImg = 'templates/ms/img/favicon/android-chrome-512x512.png';
	const ERROR_500 = 0; // 0 = то во всею страницу, 1 = Вариант page-500_2
	const ERROE_500_info = 0; // Отодбражать дополнительную информацию
	const ERROE_500_error = 0; // Отображать содержимое ошибки
	const ERROR_404 = 0; // 0 = то во всею страницу, 1 Вариант page-500_2
	const JGENERATOR = 2; // 0 = Оставить, 1 = Выключить, 2 = Вывести текст
	const JBODY_classes = 0; // Показывать классы от Joomla в теге body
	const BACK_TO_TOP = 1; // Кнопка на верх
	const AWESOME_CDN = 1; // CDN где находится кит со шрифтами

	//kH1 Template positions
	const C_FOOTBAR1 = 'ms-footer-col col-lg-4'; // ms-footer-col col-lg-4
	const C_FOOTBAR2 = 'ms-footer-col col-lg-4 col-md-7 ms-footer-alt-color kn-tc'; // ms-footer-col col-lg-5 col-md-7 ms-footer-alt-color
	const C_FOOTBAR3 = 'ms-footer-col col-lg-4 col-md-5 map'; //ms-footer-col col-lg-3 col-md-5 ms-footer-text-right
	const C_FOOTBAR4 = 'ms-footer-col col-lg-4'; // ms-footer-col col-lg-4

	//kH1 Contact
	const CONTACT_InfoCard = 0; // Показывать нижний блок с информациией
	const CONTACT_Form = 0; // Заголовок формы
	const CONTACT_Info = 0; // Заголовок контакта
	const CONTACT_Misk = 0; // Заголовок дополнительно
	const CONTACT_Legend = 1; // Заголовок дополнительно

	//kH1 mod_articles_category
	const ARTICLES_Header = 0; // Отображать информацию над картинкой
	const ARTICLES_LinkImg = 1; // Ссылка на материал из картинки.
	const ARTICLES_CardImgChe = 1; // Картинки в карточкаш в шахматном порядке. 0=Выкл; 1=Картинка с лева; 2=картинка с права.
	const ARTICLES_CatDesc = 0; // Описание категории
}

Class KnTemplate
{
	//kH1 Template
	const BURGER_LogoCircle = 0; // Отображение кружка перед Логотипом
	const MAIN_MENU_LogoCircle = 0;
	const BURGER_LogoImg = 'templates/ms/img/favicon/android-chrome-512x512.png'; // Изображение перед логотипом
	const MAIN_MENU_LogoImg = 'templates/ms/img/favicon/android-chrome-512x512.png';
	const ERROR_500 = 0; // 0 = то во всею страницу, 1 = Вариант page-500_2
	const ERROE_500_info = 0; // Отодбражать дополнительную информацию
	const ERROE_500_error = 0; // Отображать содержимое ошибки
	const ERROR_404 = 0; // 0 = то во всею страницу, 1 Вариант page-500_2
	const JGENERATOR = 2; // 0 = Оставить, 1 = Выключить, 2 = Вывести текст
	const JBODY_classes = 0; // Показывать классы от Joomla в теге body
	const BACK_TO_TOP = 1; // Кнопка на верх
	const AWESOME_CDN = 0; // CDN где находится кит со шрифтами

	//kH1 Template positions
	const C_FOOTBAR1 = 'ms-footer-col col-lg-4'; // ms-footer-col col-lg-4
	const C_FOOTBAR2 = 'ms-footer-col col-lg-4 col-md-7 ms-footer-alt-color kn-tc'; // ms-footer-col col-lg-5 col-md-7 ms-footer-alt-color
	const C_FOOTBAR3 = 'ms-footer-col col-lg-4 col-md-5 map'; //ms-footer-col col-lg-3 col-md-5 ms-footer-text-right
	const C_FOOTBAR4 = 'ms-footer-col col-lg-4'; // ms-footer-col col-lg-4
}


//kH1 com_content_category
Class ComContentCategory
{
	const HEADER_Css = 'no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5'; // Стили заголовка
	const HEADER_Img = '/templates/ms/img/coffee.jpg';
	const LINK_Box = 0; // Блок ссылок под текстом
	const FILTER_Btn = 0; // Кнопка сброса фильтрации
}


Class ComContentArticle // $S_ComContentArticle = new ComContentArticle();
{
	const INFO_BLOCK_portfolioItems2 = 0; // Информация о материале
}

Class MsTemplate
{
	// Фоновое изображение шапки
	public static function imgBgLink($img, $j=0){
		if ($j == 1){ // Изображение полного текста
			$img = strstr(json_decode($img)->image_fulltext, '#', true);
		}
		if ($j == 2){ // Изображение вводного текста
			$img = strstr(json_decode($img)->image_intro, '#', true);
		}
		if ($j == 3){ // Изображение обрезать до знака
			$img = strstr($img, '#', true);
		}
		if ($img){
			return 'style="background-image: url(' . $img . ');"';
		}else{
			return '';
		}
	}
}
