$('.form-select').addClass('selectpicker form-control');

// FAB2 MS-140
$('fab')
  .on('click', 'button', (e) => {
    const $del = $(e.delegateTarget);
    $del.toggleClass('open');
});

// MS-137
if ($('.mod-breadcrumbs__wrapper') && $('.ms-hero-page-override')){
  $('.mod-breadcrumbs__wrapper').addClass('hero-fix')
}