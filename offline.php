<?php
/**
 * @package     Templates.ms
 * @subpackage  404
 *
 * @task        MS-52, MS-142
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Helper\AuthenticationHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;


require_once (JPATH_ROOT.'/templates/ms/settings.php');

/** @var Joomla\CMS\Document\HtmlDocument $this */

$twofactormethods = AuthenticationHelper::getTwoFactorMethods();
$extraButtons     = AuthenticationHelper::getLoginButtons('form-login');
$app              = Factory::getApplication();
$wa               = $this->getWebAssetManager();

$fullWidth = 1;

// Template path
$templatePath = 'templates/' . $this->template;

// Color Theme
$paramsColorName = $this->params->get('colorName', 'colors_standard');
$assetColorName  = 'theme.' . $paramsColorName;
//$wa->registerAndUseStyle($assetColorName, $templatePath . '/css/global/' . $paramsColorName . '.css');

// Enable assets
$wa->useStyle('template.active.language')
	->useStyle('template.user')
	->useScript('template.user')
	->useScript('template.app');

// Override 'template.active' asset to set correct ltr/rtl dependency
$wa->registerStyle('template.active', '', [], [], ['template.ms-css']);

// Logo file or site title param
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');

if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . htmlspecialchars(Uri::root() . $this->params->get('logoFile'), ENT_QUOTES, 'UTF-8') . '" alt="' . $sitename . '">';
}
elseif ($this->params->get('siteTitle'))
{
	$logo = '<span title="' . $sitename . '">' . htmlspecialchars($this->params->get('siteTitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = '';
}

// Defer font awesome
$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');


// Фоновое изображение
if ($app->get('offline_image')){
	$imgBgLink = MsTemplate::imgBgLink($app->get('offline_image'), 3);
}else{
	$imgBgLink = '';
}


// Head
//require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/html/layouts/ms/head.php');

// JGenerator
if (KnTemplate::JGENERATOR === 1) {
	$wa = $this->setGenerator(null);
} elseif (KnTemplate::JGENERATOR === 2) {
	$wa = $this->setGenerator('KaenStudio');
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="ltr">
<head>
	<jdoc:include type="metas" />
	<jdoc:include type="styles" />
	<jdoc:include type="scripts" />
  <style>
    .ms-footer{position:fixed;bottom:0;width:100%;z-index:1;}
    .btn-circle{position:fixed;bottom:3rem;right:3rem;}
  </style>
</head>
<body class="offline">
  <div class="bg-full-page ms-hero-img ms-hero-bg-primary ms-bg-fixed back-fixed" <?= $imgBgLink ?>>
    <div class="absolute-center">
      <div class="container">
        <div class="row">
          <div class="col">
	          <?php if (!empty($logo)) : ?>
              <h1><?php echo $logo; ?></h1>
	          <?php else : ?>
              <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5 kn-tc"><?php echo $sitename; ?></h1>
	          <?php endif; ?>
	          <?php if ($app->get('display_offline_message', 1) == 1 && str_replace(' ', '', $app->get('offline_message')) != '') : ?>
              <p class="lead lead-xl mt-2 animated fadeInUp animation-delay-7 kn-tc color-white"><?php echo $app->get('offline_message'); ?></p>
	          <?php elseif ($app->get('display_offline_message', 1) == 2) : ?>
              <p class="lead lead-xl mt-2 animated fadeInUp animation-delay-7 kn-tc color-white"><?php echo Text::_('JOFFLINE_MESSAGE'); ?></p>
	          <?php endif; ?>
            <jdoc:include type="message" />
          </div>
        </div>
      </div>
      <jdoc:include type="modules" name="offline" style="card"/>


    </div>
    <jdoc:include type="modules" name="copyright" style="none"/>
    <a href="#" class="btn-circle btn-circle-raised btn-circle-primary"><i class="zmdi zmdi-account"></i></a>
  </div>
  <div id="knLogin" class="card absolute-center col-md-6 index-2 animated zoomInRight kn-n">
    <div class="card-body-big">
      <form action="<?php echo Route::_('index.php', true); ?>" method="post" id="form-login">
        <fieldset>
          <label for="username"><?php echo Text::_('JGLOBAL_USERNAME'); ?></label>
          <input name="username" class="form-control" id="username" type="text">

          <label for="password"><?php echo Text::_('JGLOBAL_PASSWORD'); ?></label>
          <input name="password" class="form-control" id="password" type="password">

			    <?php if (count($twofactormethods) > 1) : ?>
            <label for="secretkey"><?php echo Text::_('JGLOBAL_SECRETKEY'); ?></label>
            <input name="secretkey" autocomplete="one-time-code" class="form-control" id="secretkey" type="text">
			    <?php endif; ?>

			    <?php foreach($extraButtons as $button):
				    $dataAttributeKeys = array_filter(array_keys($button), function ($key) {
					    return substr($key, 0, 5) == 'data-';
				    });
				    ?>
            <div class="mod-login__submit form-group">
              <button type="button"
                      class="btn btn-secondary w-100 mt-4 <?php echo $button['class'] ?? '' ?>"
					    <?php foreach ($dataAttributeKeys as $key): ?>
						    <?php echo $key ?>="<?php echo $button[$key] ?>"
					    <?php endforeach; ?>
					    <?php if ($button['onclick']): ?>
                onclick="<?php echo $button['onclick'] ?>"
					    <?php endif; ?>
              title="<?php echo Text::_($button['label']) ?>"
              id="<?php echo $button['id'] ?>"
              >
					    <?php if (!empty($button['icon'])): ?>
                <span class="<?php echo $button['icon'] ?>"></span>
					    <?php elseif (!empty($button['image'])): ?>
						    <?php echo $button['image']; ?>
					    <?php elseif (!empty($button['svg'])): ?>
						    <?php echo $button['svg']; ?>
					    <?php endif; ?>
					    <?php echo Text::_($button['label']) ?>
              </button>
            </div>
			    <?php endforeach; ?>

          <input type="submit" name="Submit" class="btn btn-primary" value="<?php echo Text::_('JLOGIN'); ?>">
          <input type="hidden" name="option" value="com_users">
          <input type="hidden" name="task" value="user.login">
          <input type="hidden" name="return" value="<?php echo base64_encode(Uri::base()); ?>">
			    <?php echo HTMLHelper::_('form.token'); ?>
        </fieldset>
      </form>
    </div>
  </div>
  <script>
    let knLogin = $( "#knLogin" );
    $('.btn-circle').click(function(){knLogin.toggleClass('kn-n')})
    $(document).on('keydown', function(e) {
      if (e.key == "Escape") {
        knLogin.addClass('kn-n')
      }
    });
    $(document).mousedown(function(e){
      if (!knLogin.hasClass('kn-n')){
        if (!knLogin.is(e.target) && knLogin.has(e.target).length === 0) knLogin.addClass('kn-n')
        }});
  </script>
  <script src="/templates/ms/js/pages/coming.js"></script>
</body>
</html>
