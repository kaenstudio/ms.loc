<?php

/**
 * Точка входа
 * @package             Joomla.Site
 * @subpackage          Templates.ms
 * @license             KaenStudio
 * @copyright       (C) 2022 <https://kaenstudio.com>
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;

use Joomla\CMS\HTML\HTMLHelper;


/** @var Joomla\CMS\Document\HtmlDocument $this */

$app = Factory::getApplication();
$wa = $this->getWebAssetManager(); //Здесь вызывается generator(в конце страницы его отключаю)


// Подключаем настройки
require_once(JPATH_ROOT . '/templates/' . $app->getTemplate() . '/settings.php');
$knS = new KnSettings();

// Подключение параметров шаблона
$knHeaderType = $this->params->get('headerType', 'primary-navbar-primary');
$knPreloader = $this->params->get('knPreloader', 1);
$knClickDropMenu = $this->params->get('knClickDropMenu');
$knSafariFavicon = $this->params->get('safariFavicon', 0);
$knAppleWebAppFavicon = $this->params->get('appleWebAppFavicon', 0);
$knWebAppFavicon = $this->params->get('webAppFavicon', 0);
$knMsTileColorFavicon = $this->params->get('msTileColorFavicon', 0);
$knThemeColorFavicon = $this->params->get('themeColorFavicon', 0);
$knBurgerDesctop = $this->params->get('burgerDesctop');
$knBurgerLogo = $this->params->get('burgerLogo');
$knJQuery = $this->params->get('jquery');


// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');
$menu = $app->getMenu()->getActive();
$pageclass = $menu !== null ? $menu->getParams()->get('pageclass_sfx', '') : '';


// Template path
$templatePath = 'templates/' . $this->template;


// Color Theme - Идея сделать переключения типов шапки
if ($knHeaderType === 'kn-theme') {
	$wa->usePreset('template.ms-css');
} else { // Пока еще не нрастроено
	$assetColorName = 'theme.' . $knHeaderType;
	$wa->registerAndUseStyle($assetColorName, $templatePath . '/css/themes/' . $knHeaderType . '.css');
}


// Enable assets
$wa->useStyle('template.active.language')
	->useStyle('template.user')
	->useScript('template.user')
	->useScript('template.app');


// Override 'template.active' asset to set template.ms-css dependency
$wa->registerStyle('template.active', '', [], [], ['template.ms-css']);


// Добавление font awesome CDN
if ($knS::AWESOME_CDN) {
	$wa->useScript('awesome.cdn');
}

// Подключение скрпита для проетка. Нужно сздать файл user.js в папке js
if (false) { // todo Доделать
	$wa->useScript('user.js');
}


// Подключение jQuery
// jQuery от Joomla
if ($knJQuery == 1) HTMLHelper::_('jquery.framework');

// jQuery от шаблона
elseif ($knJQuery == 2) $wa->useScript('jquery.js');

// jQuery из CDN
elseif ($knJQuery == 3) $wa->registerAndUseScript('CDN', 'https://code.jquery.com/jquery-3.6.0.min.js',  [], ['integrity' => 'sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=', 'crossorigin' => 'anonymous']);


// Logo file or site title param Для главного меню
if ($this->params->get('logoFile')) {
	$logo = '<img src="' . Uri::root(true) . '/' . htmlspecialchars($this->params->get('logoFile'), ENT_QUOTES) . '" alt="' . $sitename . '">';
} elseif ($this->params->get('siteTitle')) {
	$siteTitle = $this->params->get('siteTitle');
	$siteDescription = '';
	if ($this->params->get('siteDescription')) {
		$siteDescription = '<strong>' . $this->params->get('siteDescription') . '</strong>';
	}
	$logo = '<span class="ms-title" title="' . $sitename . '">' . $siteTitle . $siteDescription . '</span>';
	// Отображение кружка в начале
	if ($knS::MAIN_MENU_LogoCircle) {
		$logo = '<span class="ms-logo ms-logo-sm">' . mb_substr($siteTitle, 0, 1) . '</span>' . $logo;
	} elseif ($knS::MAIN_MENU_LogoImg) {
		$logo = '<img src="' . $knS::MAIN_MENU_LogoImg . '" class="kn-small-logo-img">' . $logo;
	}
} else {
	$logo = '';
}


// Лого для бургера
if ($this->params->get('logoFile')) {
	$burgerlogo = '<img src="' . Uri::root(true) . '/' . htmlspecialchars($this->params->get('logoFile'), ENT_QUOTES) . '" alt="' . $sitename . '">';
} elseif ($this->params->get('siteTitle')) {
	$siteTitle = $this->params->get('siteTitle');
	$siteDescription = '';
	if ($this->params->get('siteDescription')) {
		$siteDescription = '<span>' . $this->params->get('siteDescription') . '</span>';
	}

	$burgerlogo = '<h3>' . $siteTitle . $siteDescription . '</h3>';

	// Отображение кружка в начале
	if ($knS::BURGER_LogoCircle) {
		$burgerlogo = '<span class="ms-logo ms-logo-sm">' . mb_substr($siteTitle, 0, 1) . '</span>' . $burgerlogo;
	} elseif ($knS::BURGER_LogoImg) {
		$burgerlogo = '<img src="' . $knS::MAIN_MENU_LogoImg . '" class="kn-small-burger-logo-img">' . $burgerlogo;
	}
} else {
	$burgerlogo = '';
}


$hasClass = ''; // todo Возможно будет ненуже

if ($this->countModules('sidebar-left', true)) {
	$hasClass .= ' has-sidebar-left';
}

if ($this->countModules('sidebar-right', true)) {
	$hasClass .= ' has-sidebar-right';
}


// Container // todo Можно использовать для контенйнера
//$wrapper = $this->params->get('fluidContainer') ? 'wrapper-fluid' : 'wrapper-static';


// todo Возможно будет ненуже
$stickyHeader = $this->params->get('stickyHeader') ? 'position-sticky sticky-top' : '';


// Defer font awesome // todo Возможно будет ненуже
//$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');


//Изображение загрузки
if ($knPreloader == 1) {
	$wa->useStyle('template.preloader');
}


// Подготовка стилей шапки
switch ($knHeaderType) {
	case 'dark-navbar-dark':
		$knHeaderTypeClass = 'ms-header-dark';
		$knHeaderTypeNavClasses = 'ms-navbar-dark';
		break;
	case 'dark-navbar-primary':
		$knHeaderTypeClass = 'ms-header-dark';
		$knHeaderTypeNavClasses = 'ms-navbar-primary';
		break;
	case 'navbar-primary':
		$knHeaderTypeNavClasses = 'ms-navbar-primary navbar-mode';
		break;
	default: // primary-navbar-primary
		$knHeaderTypeClass = 'ms-header-primary';
		$knHeaderTypeNavClasses = 'ms-navbar-primary';
		break;
}


// Head
require_once(JPATH_ROOT . '/templates/' . $app->getTemplate() . '/html/layouts/ms/head.php');


// Body classes
if ($knS::JBODY_classes) {
	$knBodyClasses = 'class="' . $option . ' ' . ' view-' . $view . ($layout ? ' layout-' . $layout : '') . ($task ? ' task-' . $task : '') . ($itemid ? ' itemid-' . $itemid : '') . ($pageclass ? ' ' . $pageclass : '') . $hasClass . '"';
}


// JGenerator
if ($knS::JGENERATOR === 1) {
	$wa = $this->setGenerator(null);
} elseif ($knS::JGENERATOR === 2) {
	$wa = $this->setGenerator('KaenStudio');
}

//print_r($_COOKIE);
//setcookie ("cookie1", 'Значение');
//echo $_COOKIE["cookie1"];

?>
<!DOCTYPE html>
<html lang="<?= $this->language; ?>" dir="ltr">
<head>
  <jdoc:include type="metas"/>
  <jdoc:include type="styles"/>
  <jdoc:include type="scripts"/>
</head>
<pre style="display: none;">
<?php
//print_r($wa);
//print_r($this->getHeadData());// Шапка
//print_r($this->getHeadData());// Шапка
//print_r($app->getDocument());
//print_r($this->mergeHeadData())
//print_r($this)
//var_dump($GLOBALS);
?>
</pre>

<body <?= !empty($knBodyClasses) ? $knBodyClasses : '' ?>>


<?php if ($knPreloader == 1) : ?>
  <div id="ms-preload" class="ms-preload">
    <div id="status">
      <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
      </div>
    </div>
  </div>
<?php endif; ?>


<div class="ms-site-container">
  <nav class="navbar navbar-expand-md navbar-static ms-navbar <?= $knHeaderTypeNavClasses ?>">
    <div class="container container-full">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?= $this->baseurl; ?>">
					<?php if ($this->countModules('headerLogo', true)) : ?>
            <jdoc:include type="modules" name="headerLogo" style="none"/>
					<?php elseif ($this->params->get('brand', 1)) : ?>
						<?= $logo; ?>
					<?php endif; ?>
        </a>
      </div>
			<?php if ($this->countModules('mainMenu', true)) : ?>
        <div class="collapse navbar-collapse" id="ms-navbar">
          <jdoc:include type="modules" name="mainMenu" style="none"/>
        </div>
			<?php endif; ?>
			<?php if ($knBurgerDesctop) : ?>
        <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu"><i class="zmdi zmdi-menu"></i></a>
			<?php endif; ?>
    </div>
  </nav>
  <jdoc:include type="modules" name="breadcrumbs" style="none"/>
  <jdoc:include type="modules" name="main-top" style="card"/>
  <jdoc:include type="message"/>
  <jdoc:include type="modules" name="main-slider" style="card"/>
  <jdoc:include type="modules" name="top1" style="card"/>
  <jdoc:include type="modules" name="top2" style="card"/>
  <jdoc:include type="modules" name="search" style="none"/>
  <jdoc:include type="component"/>
  <jdoc:include type="modules" name="main-bottom" style="card"/>
  <jdoc:include type="modules" name="bottom-a" style="card"/>
  <jdoc:include type="modules" name="bottom-b" style="card"/>
  <jdoc:include type="modules" name="bottom-c" style="card"/>
</div>
<?php if ($this->countModules('footbar1', true)) : ?>
  <footer class="ms-footbar">
    <div class="container">
      <div class="row">
				<?php if ($this->countModules('footbar1', true)) : ?>
          <div class="<?= $knS::C_FOOTBAR1 ?>">
            <jdoc:include type="modules" name="footbar1" style="footbar-block"/>
          </div>
				<?php endif; ?>
				<?php if ($this->countModules('footbar2', true)) : ?>
          <div class="<?= $knS::C_FOOTBAR2 ?>">
            <jdoc:include type="modules" name="footbar2" style="footbar-block"/>
          </div>
				<?php endif; ?>
				<?php if ($this->countModules('footbar3', true)) : ?>
          <div class="<?= $knS::C_FOOTBAR3 ?>">
            <jdoc:include type="modules" name="footbar3" style="footbar-block"/>
          </div>
				<?php endif; ?>
				<?php if ($this->countModules('footbar4', true)) : ?>
          <div class="<?= $knS::C_FOOTBAR4 ?>">
            <jdoc:include type="modules" name="footbar4" style="footbar-block"/>
          </div>
				<?php endif; ?>
      </div>
    </div>
  </footer>
<?php endif; ?>
<?php if ($this->countModules('copyright', true)) : ?>
  <jdoc:include type="modules" name="copyright" style="none"/>
<?php else : ?>
  <footer class="ms-footer">
    <div class="container">
      <p><?= Text::_('MS_COPYRIGHT') ?> &copy; <?= $sitename . ' ' . date('Y') ?></p>
    </div>
  </footer>
<?php endif; ?>
<?php if ($this->params->get('backTop') == 1) : ?>
  <div class="btn-back-top">
    <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised "><i
          class="zmdi zmdi-long-arrow-up"></i></a>
  </div>
<?php endif; ?>
<?php if ($this->countModules('fab', true)) : ?>
  <jdoc:include type="modules" name="fab" style="footbar-block"/>
<?php endif; ?>
<?php if ($this->countModules('burgerMenu', true)) : ?>
  <!-- kn-site-container -->
  <div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
    <div class="sb-slidebar-container">
			<?php if ($this->countModules('burgerMenuLogin', true) || $this->countModules('burgerMenuSearch', true) || $knBurgerLogo) : ?>
        <header class="ms-slidebar-header">
					<?php if ($this->countModules('burgerMenuLogin', true)) : ?>
					<?php endif; ?>
					<?php if ($this->countModules('burgerMenuSearch', true) || $knBurgerLogo) : ?>
            <div class="ms-slidebar-title">
							<?php if ($this->countModules('burgerMenuSearch', true)) : ?>
							<?php endif; ?>
							<?php if ($knBurgerLogo) : ?>
                <div class="ms-slidebar-t">
									<?php echo $burgerlogo; ?>
                </div>
							<?php endif; ?>
            </div>
					<?php endif; ?>
        </header>
			<?php endif; ?>
      <jdoc:include type="modules" name="burgerMenu" style="none"/>
			<?php if ($this->countModules('burgerSocial', true)) : ?>
        <jdoc:include type="modules" name="burgerSocial" style="burgerSocial"/>
			<?php endif; ?>
    </div>
  </div>
<?php endif; ?>
<?php if ($this->countModules('cookie', true)) : ?>
  <jdoc:include type="modules" name="cookie" style="footbar-block"/>
<?php endif; ?>
<jdoc:include type="modules" name="debug" style="none"/>
</body>
</html>
