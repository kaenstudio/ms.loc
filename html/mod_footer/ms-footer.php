<?php
/**
 * @package     MS
 * @subpackage  mod_footer
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

?>
<footer class="ms-footer">
    <div class="container">
        <p><?php echo $lineone; ?></p>
    </div>
</footer>
