<?php
/**
 * @package     MS
 * @subpackage  Head
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */


// Отображение на экране
$this->setMetaData('X-UA-Compatible', 'IE=edge', true);
$this->setMetaData('viewport', 'width=device-width, initial-scale=1');
$this->setMetaData('HandheldFriendly', 'true');
$this->setMetaData('MobileOptimized', 'width');
$this->setMetaData('apple-mobile-web-app-capable', 'yes');


// Иконки
if ($knAppleWebAppFavicon) $this->setMetaData('apple-mobile-web-app-title', $knAppleWebAppFavicon);
if ($knWebAppFavicon) $this->setMetaData('application-name', $knWebAppFavicon);
if ($knMsTileColorFavicon) $this->setMetaData('msapplication-TileColor', $knMsTileColorFavicon);
$this->setMetaData('msapplication-config', 'templates/ms/img/favicon/browserconfig.xml');
if ($knThemeColorFavicon) $this->setMetaData('theme-color', $knThemeColorFavicon);



// Favicons
$this->addHeadLink($templatePath.'/img/favicon/apple-touch-icon.png', 'apple-touch-icon', 'rel', ['size' => '180x180']);
$this->addHeadLink($templatePath.'/img/favicon/favicon-32x32.png', 'icon', 'rel', ['size' => '32x32', 'type' => 'image/png']);
$this->addHeadLink($templatePath.'/img/favicon/favicon-16x16.png', 'icon', 'rel', ['size' => '16x16', 'type' => 'image/png']);
$this->addHeadLink($templatePath.'/img/favicon/site.webmanifest', 'manifest', 'rel');
if ($knSafariFavicon){
	$this->addHeadLink($templatePath.'/img/favicon/safari-pinned-tab.svg', 'mask-icon', 'rel', ['color' => ''.$knSafariFavicon.'']);
}
$this->addHeadLink($templatePath.'/img/favicon/favicon.ico', 'shortcut icon', 'rel');

// Шрифты
//$this->addHeadLink('https://fonts.googleapis.com/icon?family=Material+Icons', 'stylesheet', 'rel');


// if IE
$ifIe = '<!--[if lt IE 9]>';
$ifIe .= "\n<script src=\"$templatePath/js/html5shiv.min.js\"></script>\n";
$ifIe .= "<script src=\"$templatePath/js/respond.min.js\"></script>\n";
$ifIe .= '<![endif]-->';
$this->addCustomTag($ifIe);