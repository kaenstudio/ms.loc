<?php
/**
 * @package     Templates.ms
 * @subpackage  Error
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

$knDebug = ($this->debug ? 'kn-n' : '');
?>
<div class="bg-full-page bg-primary <?php echo $knDebug ?>">
    <div class="mw-500 absolute-center">
        <div class="card animated zoomInUp animation-delay-7 color-primary withripple">
            <div class="card-body">
	            <?php if ($knS::ERROE_500_info) : ?>
                <div class="text-center color-dark kn-500">
                    <h1 class="color-primary text-big"><?php echo Text::_('JERROR_LAYOUT_PAGE_NOT_FOUND'); ?></h1>
                    <jdoc:include type="message" />
                    <h2 class="color-danger"><?php echo Text::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></h2>
                    <p class="lead lead-sm"><?php echo Text::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></p>
                    <ul>
                        <li><?php echo Text::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
                        <li><?php echo Text::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
                        <li><?php echo Text::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
                        <li><?php echo Text::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
                    </ul>
                    <p class="color-success"><?php echo Text::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?></p>

                <?php else : ?>
                <div class="text-center color-dark">
                    <h1 class="color-primary text-big"><?php echo Text::_('MS_ERROR_500'); ?></h1>
                    <h2><?php echo Text::_('MS_ERROR_500_TYPE'); ?></h2>
                    <p class="lead lead-sm"><?php echo Text::_('MS_ERROR_500_INFO'); ?></p>
                <?php endif; ?>

                    <a href="<?php echo $this->baseurl; ?>/index.php" class="btn btn-primary btn-raised"><i class="zmdi zmdi-home"></i> <?php echo Text::_('JERROR_LAYOUT_HOME_PAGE'); ?></a>

                    <?php if ($knS::ERROE_500_error) : ?>
                    <hr>
                    <p><?php echo Text::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></p>
                    <blockquote class="color-danger-inverse">
                        <span class="badge bg-secondary"><?php echo $this->error->getCode(); ?></span> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?>
                    </blockquote>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($this->debug) : ?>
	<div class="site-grid">
		<div class="grid-child container-component">
			<div class="card">
				<div class="card-body">
                <?php echo $this->renderBacktrace(); ?>
                <?php // Check if there are more Exceptions and render their data as well ?>
                <?php if ($this->error->getPrevious()) : ?>
                    <?php $loop = true; ?>
                    <?php // Reference $this->_error here and in the loop as setError() assigns errors to this property and we need this for the backtrace to work correctly ?>
                    <?php // Make the first assignment to setError() outside the loop so the loop does not skip Exceptions ?>
                    <?php $this->setError($this->_error->getPrevious()); ?>
                    <?php while ($loop === true) : ?>
                        <p><strong><?php echo Text::_('JERROR_LAYOUT_PREVIOUS_ERROR'); ?></strong></p>
                        <p><?php echo htmlspecialchars($this->_error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></p>
                        <?php echo $this->renderBacktrace(); ?>
                        <?php $loop = $this->setError($this->_error->getPrevious()); ?>
                    <?php endwhile; ?>
                    <?php // Reset the main error object to the base error ?>
                    <?php $this->setError($this->error); ?>
                <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>