<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Factory;

require_once (JPATH_ROOT.'/templates/'.Factory::getApplication()->getTemplate().'/settings.php');
$knS = new KnSettings();

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.formvalidator');
?>

<form id="contact-form" action="<?php echo Route::_('index.php'); ?>" method="post" class="form-validate form-horizontal">
    <?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
        <?php if ($fieldset->name === 'captcha' && !$this->captchaEnabled) : ?>
            <?php continue; ?>
        <?php endif; ?>
        <?php $fields = $this->form->getFieldset($fieldset->name); ?>
        <?php if (count($fields)) : ?>
            <fieldset class="container">
                <?php if (isset($fieldset->label) && ($legend = trim(Text::_($fieldset->label))) !== '' && $knS::CONTACT_Legend) : ?>
                    <legend><?php echo $legend; ?></legend>
                <?php endif; ?>
                <?php foreach ($fields as $field) : ?>
                    <?php echo $field->renderField(); ?>
                <?php endforeach; ?>
            </fieldset>
        <?php endif; ?>
    <?php endforeach; ?>
    <div class="form-group row justify-content-end">
        <div class="col-lg-10">
            <button class="btn btn-raised btn-primary validate" type="submit"><?php echo Text::_('COM_CONTACT_CONTACT_SEND'); ?></button>
            <input type="hidden" name="option" value="com_contact">
            <input type="hidden" name="task" value="contact.submit">
            <input type="hidden" name="return" value="<?php echo $this->return_page; ?>">
            <input type="hidden" name="id" value="<?php echo $this->item->slug; ?>">
            <?php echo HTMLHelper::_('form.token'); ?>
        </div>
    </div>
</form>
<script>
$('label.required').on("DOMNodeInserted", function (event) {
    console.log('ura2')

});
</script>
