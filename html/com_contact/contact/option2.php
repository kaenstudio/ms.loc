<?php
/**
 * @package     MS
 * @subpackage  com_contact
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Router\Route;
use Joomla\Component\Contact\Site\Helper\RouteHelper;

require_once (JPATH_ROOT.'/templates/'.Factory::getApplication()->getTemplate().'/settings.php');
$knS = new KnSettings();


$tparams = $this->item->params;
$canDo   = ContentHelper::getActions('com_contact', 'category', $this->item->catid);
$canEdit = $canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by === Factory::getUser()->id);
$htag    = $tparams->get('show_page_heading') ? 'h2' : 'h1';


$contactPos = 0;
$contactPos1 = 0;
$contactPos2 = 0;
$contactPos3 = 0;
$contactHeaderImg = '';


foreach ($this->item->jcfields as $key => $value)
{
	// Шапка
    if ($value->name === 'contact-header' && $value->value)
	{
		$contactHeader = $value->value;
		//$noHeader = 1;
	}

    // Изображение шапки
	if ($value->name === 'contact-header-img' && $value->value)
	{
		$contactHeaderImg = 'style="background-image: url('.$value->value.');"';
	}
	else{
		$contactHeaderImg = '';
    }

	// Карта
	if ($value->name === 'contact-map' && $value->value)
    {
        $contactMap = $value->value;
    }

	// Активные позиции 2й карточки
	if ($value->name === 'contact-pos')
	{
		$contactPos = 1;
		if (in_array(1, $value->rawvalue)){
			$contactPos1 = 1;
        }
		if (in_array(2, $value->rawvalue)){
			$contactPos2 = 1;
		}
		if (in_array(3, $value->rawvalue)){
			$contactPos3 = 1;
		}
	}
}


// Классы для позиции
if ($contactPos){
    if ($contactPos1 && $contactPos2 && $contactPos3){
        $posClass1 = 'col-lg-4 col-md-6';
        $posClass2 = 'col-lg-4 col-md-6';
        $posClass3 = 'col-lg-4 col-md-12';
    }
    elseif ($contactPos1 && $contactPos3){
	    $posClass1 = 'col-lg-4 col-md-5';
	    $posClass3 = 'col-lg-8 col-md-7';
    }
    else{
	    $posClass1 = 'col-md-6';
	    $posClass2 = 'col-md-6';
    }
}
?>
<div class="ms-hero-page-override ms-hero-img-team ms-hero-bg-primary" data-kn="option2" <?= $contactHeaderImg ?>>
    <div class="container">
        <div class="text-center">
            <?php
                if (!empty($contactHeader))
                {
                    echo $contactHeader;
                }
                elseif (empty($contactHeader) && $tparams->get('show_page_heading'))
                {
	                echo '<h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">'.$this->escape($tparams->get('page_heading')).'</h1>';
                }
            ?>
        </div>
    </div>
</div>
<div class="container" itemscope itemtype="https://schema.org/Person" data-kn="com-contact contact">
<?php if ($tparams->get('show_email_form') && ($this->item->email_to || $this->item->user_id)) : ?>
    <div class="card card-hero animated fadeInUp animation-delay-7">
        <div class="card-body contact-form">
        <?= $knS::CONTACT_Form ? '<h3 class="color-primary text-center">' . Text::_('COM_CONTACT_EMAIL_FORM') . '</h3>' : ''; ?>

        <?= $this->loadTemplate('form'); ?>
        </div>
    </div>
<?php endif; ?>
<?php if ($knS::CONTACT_InfoCard) : ?>
    <div class="card card-primary animated fadeInUp animation-delay-9">

	<?= $this->item->event->afterDisplayTitle; ?>

    <?php if ($this->item->name && $tparams->get('show_name')) : ?>
        <div class="card-header">
            <<?= $htag; ?>>
		    <?php if ($this->item->published == 0) : ?>
                <span class="badge bg-warning text-light"><?= Text::_('JUNPUBLISHED'); ?></span>
		    <?php endif; ?>
            <span class="card-title" itemprop="name"><?= $this->item->name; ?></span>
        </<?= $htag; ?>>
    </div>
<?php endif; ?>

    <div class="row">
    <?php if ($contactPos1) : ?>
    <div class="<?= $posClass1; ?>">
    <?php if ($this->params->get('show_info', 1)) : ?>
        <?= $knS::CONTACT_Info ? '<h3 class="color-primary text-center">' . Text::_('COM_CONTACT_DETAILS') . '</h3>' : ''; ?>
        <div class="card-body kn-contact-resp">
            <?php if ($this->item->image && $tparams->get('show_image')) : ?>
                <?= LayoutHelper::render(
                    'joomla.html.image',
                    [
                        'src'      => $this->item->image,
                        'alt'      => $this->item->name,
                        'itemprop' => 'image',
                        'class'    => 'kn-contact-img'
                    ]
                ); ?>
            <?php endif; ?>
            <?php if ($this->item->con_position && $tparams->get('show_position')) : ?>
                <dl class="com-contact__position contact-position dl-horizontal">
                    <dt><?= Text::_('COM_CONTACT_POSITION'); ?>:</dt>
                    <dd itemprop="jobTitle">
                        <?= $this->item->con_position; ?>
                    </dd>
                </dl>
            <?php endif; ?>
            <?= $this->loadTemplate('address'); ?>

            <?php if ($tparams->get('allow_vcard')) : ?>
                <?= Text::_('COM_CONTACT_DOWNLOAD_INFORMATION_AS'); ?>
                <a href="<?= Route::_('index.php?option=com_contact&amp;view=contact&amp;id=' . $this->item->id . '&amp;format=vcf'); ?>">
                    <?= Text::_('COM_CONTACT_VCARD'); ?></a>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php if ($contactPos2) : ?>
    <div class="<?= $posClass2; ?>">
        <?php if ($this->item->misc && $tparams->get('show_misc')) : ?>
            <?= $knS::CONTACT_Misk ? '<h3 class="color-primary text-center">' . Text::_('COM_CONTACT_OTHER_INFORMATION') . '</h3>' : ''; ?>
            <div class="card-body contact-miscinfo">
                <?= $this->item->misc; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php if ($contactPos3) : ?>
    <div class="<?= $posClass3; ?>">
        <?= $contactMap; ?>
    </div>
    <?php endif; ?>

    <?php if ($tparams->get('show_contact_list') && count($this->contacts) > 1) : ?>
        <form action="#" method="get" name="selectForm" id="selectForm">
            <label for="select_contact"><?= Text::_('COM_CONTACT_SELECT_CONTACT'); ?></label>
            <?= HTMLHelper::_(
                'select.genericlist',
                $this->contacts,
                'select_contact',
                'class="form-select" onchange="document.location.href = this.value"', 'link', 'name', $this->item->link);
            ?>
        </form>
    <?php endif; ?>

    <?php if ($tparams->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
        <div class="com-contact__tags">
            <?php $this->item->tagLayout = new FileLayout('joomla.content.tags'); ?>
            <?= $this->item->tagLayout->render($this->item->tags->itemTags); ?>
        </div>
    <?php endif; ?>

    <?= $this->item->event->beforeDisplayContent; ?>

    <?php if ($tparams->get('show_links')) : ?>
        <?= $this->loadTemplate('links'); ?>
    <?php endif; ?>

    <?php if ($tparams->get('show_articles') && $this->item->user_id && $this->item->articles) : ?>
        <?= '<h3>' . Text::_('JGLOBAL_ARTICLES') . '</h3>'; ?>

        <?= $this->loadTemplate('articles'); ?>
    <?php endif; ?>

    <?php if ($tparams->get('show_profile') && $this->item->user_id && PluginHelper::isEnabled('user', 'profile')) : ?>
        <?= '<h3>' . Text::_('COM_CONTACT_PROFILE') . '</h3>'; ?>

        <?= $this->loadTemplate('profile'); ?>
    <?php endif; ?>

    <?php if ($tparams->get('show_user_custom_fields') && $this->contactUser) : ?>
        <?= $this->loadTemplate('user_custom_fields'); ?>
    <?php endif; ?>
    </div>

	<?php if ($canEdit) : ?>
        <div class="icons">
            <div class="float-end">
                <div>
					<?= HTMLHelper::_('contacticon.edit', $this->item, $tparams); ?>
                </div>
            </div>
        </div>
	<?php endif; ?>

	<?php $show_contact_category = $tparams->get('show_contact_category'); ?>

	<?php if ($show_contact_category === 'show_no_link') : ?>
        <h3>
            <span class="contact-category"><?= $this->item->category_title; ?></span>
        </h3>
	<?php elseif ($show_contact_category === 'show_with_link') : ?>
		<?php $contactLink = RouteHelper::getCategoryRoute($this->item->catid, $this->item->language); ?>
        <h3>
			<span class="contact-category"><a href="<?= $contactLink; ?>">
				<?= $this->escape($this->item->category_title); ?></a>
			</span>
        </h3>
	<?php endif; ?>
	<?= $this->item->event->afterDisplayContent; ?>
    </div>
<?php endif; ?>
</div>
