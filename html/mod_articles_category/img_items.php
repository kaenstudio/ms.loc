<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

$i = $knS::ARTICLES_CardImgChe;
?>

<?php foreach ($items as $item) : ?>
<?php $i++;?>
<section class="card kn-maci">
    <div class="row no-gutters">
	    <?php if ($knS::ARTICLES_CardImgChe && $i%2==0) : ?>
        <div class="col-lg-6 ">
            <?= LayoutHelper::render('joomla.content.intro_image', $item); ?>
        </div>
		<?php elseif (!$knS::ARTICLES_CardImgChe) : ?>
        <div class="col-lg-6 ">
            <?= LayoutHelper::render('joomla.content.intro_image', $item); ?>
        </div>
        <?php endif; ?>
        <div class="col-lg-6 order-2 order-lg-1">
            <div class="card-body overflow-hidden text-center">
            <?php if ($params->get('link_titles') == 1) : ?>
                <?php $attributes = ['class' => 'mod-articles-category-title ' . $item->active]; ?>
                <?php $link = htmlspecialchars($item->link, ENT_COMPAT, 'UTF-8', false); ?>
                <?php $title = htmlspecialchars($item->title, ENT_COMPAT, 'UTF-8', false); ?>
              <h3 class="text-center"><?php echo HTMLHelper::_('link', $link, $title, $attributes); ?></h3>
            <?php else : ?>
              <h3 class="text-center"><?php echo $item->title; ?></h3>
            <?php endif; ?>
            <?php if ($params->get('show_introtext')) : ?>
              <div class="mod-articles-category-introtext"><?= $item->displayIntrotext; ?></div>
            <?php endif; ?>
            </div>
        </div>
	    <?php if ($knS::ARTICLES_CardImgChe && $i%2!=0) : ?>
        <div class="col-lg-6 order-1 order-lg-2">
            <?= LayoutHelper::render('joomla.content.intro_image', $item); ?>
        </div>
        <?php endif; ?>
	<?php if ($item->displayHits) : ?>
		<span class="mod-articles-category-hits">
			(<?php echo $item->displayHits; ?>)
		</span>
	<?php endif; ?>

	<?php if ($params->get('show_author')) : ?>
		<span class="mod-articles-category-writtenby">
			<?php echo $item->displayAuthorName; ?>
		</span>
	<?php endif; ?>

	<?php if ($item->displayCategoryTitle) : ?>
		<span class="mod-articles-category-category">
			(<?php echo $item->displayCategoryTitle; ?>)
		</span>
	<?php endif; ?>

	<?php if ($item->displayDate) : ?>
		<span class="mod-articles-category-date"><?php echo $item->displayDate; ?></span>
	<?php endif; ?>

	<?php if ($params->get('show_tags', 0) && $item->tags->itemTags) : ?>
		<div class="mod-articles-category-tags">
			<?php echo LayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
		</div>
	<?php endif; ?>

	<?php if ($params->get('show_readmore')) : ?>
		<p class="mod-articles-category-readmore">
			<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
				<?php if ($item->params->get('access-view') == false) : ?>
					<?php echo Text::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
				<?php elseif ($item->alternative_readmore) : ?>
					<?php echo $item->alternative_readmore; ?>
					<?php echo HTMLHelper::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
						<?php if ($params->get('show_readmore_title', 0)) : ?>
							<?php echo HTMLHelper::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
						<?php endif; ?>
				<?php elseif ($params->get('show_readmore_title', 0)) : ?>
					<?php echo Text::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
					<?php echo HTMLHelper::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
				<?php else : ?>
					<?php echo Text::_('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
				<?php endif; ?>
			</a>
		</p>
	<?php endif; ?>
</div>
</section>
<?php endforeach; ?>