<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/settings.php');
$knS = new KnSettings();

$i = 1;
?>
<?php foreach ($items as $item) : ?>
<?php
if ($params->get('link_titles') == 1) {
    $attributes = ['class' => 'kn-l ' . $item->active];
    $link = htmlspecialchars($item->link, ENT_COMPAT, 'UTF-8', false);
    $title = htmlspecialchars($item->title, ENT_COMPAT, 'UTF-8', false);
    $knTitle = HTMLHelper::_('link', $link, $title, $attributes);
} else {
    $knTitle = $item->title;
}
?>
<div class="carousel-item <?= $i ? 'active' : ''; $i = 0; ?>">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="kn-owl-slider-full-img">
	                <?php if ($knS::ARTICLES_Header) : ?>
                    <div class="kn-owl-slider-full-content">
                        <div class="carousel-caption">
                            <h1 class="color-primary no-mt animated zoomInDown animation-delay-7"><?= $knTitle ?></h1>
                            <?php if ($params->get('show_introtext')) : ?>
                            <p class="mod-articles-category-introtext">
                            <?= $item->displayIntrotext; ?>
                            </p>
                            <?php endif; ?>

                            <?php if ($item->displayHits) : ?>
                            <span class="mod-articles-category-hits">(<?= $item->displayHits; ?>)</span>
                            <?php endif; ?>

                            <?php if ($params->get('show_author')) : ?>
                            <span class="mod-articles-category-writtenby"><?= $item->displayAuthorName; ?></span>
                            <?php endif; ?>

                            <?php if ($item->displayCategoryTitle) : ?>
                            <span class="mod-articles-category-category">(<?= $item->displayCategoryTitle; ?>)</span>
                            <?php endif; ?>

                            <?php if ($item->displayDate) : ?>
                            <span class="mod-articles-category-date"><?= $item->displayDate; ?></span>
                            <?php endif; ?>

                            <?php if ($params->get('show_tags', 0) && $item->tags->itemTags) : ?>
                            <div class="mod-articles-category-tags">
                                <?= LayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
                            </div>
                            <?php endif; ?>

                            <?php if ($params->get('show_readmore')) : ?>
                            <p class="mod-articles-category-readmore">
                                <a class="mod-articles-category-title <?= $item->active; ?>" href="<?= $item->link; ?>">
                                <?php if ($item->params->get('access-view') == false) : ?>
                                    <?= Text::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
                                <?php elseif ($item->alternative_readmore) : ?>
                                    <?= $item->alternative_readmore; ?>
                                    <?= HTMLHelper::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
                                    <?php if ($params->get('show_readmore_title', 0)) : ?>
                                        <?= HTMLHelper::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
                                    <?php endif; ?>
                                <?php elseif ($params->get('show_readmore_title', 0)) : ?>
                                    <?= Text::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
                                    <?= HTMLHelper::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
                                <?php else : ?>
                                    <?= Text::_('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
                                <?php endif; ?>
                                </a>
                              </p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endif; ?>
	                <?php if ($knS::ARTICLES_LinkImg) : ?>
                    <a href="<?= $link ?>">
                    <?php endif; ?>
                    <?= LayoutHelper::render('joomla.content.full_image', $item); ?>
                    <?php if ($knS::ARTICLES_LinkImg) : ?>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>