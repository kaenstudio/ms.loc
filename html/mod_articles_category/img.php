<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 *
 * @description
 * Вывод списка материалов в виде категории
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Categories\Categories;


$categories = Categories::getInstance("content");
$rootNode = $categories->get($idbase[0]);

require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/settings.php');
$knS = new KnSettings();

if (!$list)
{
	return;
}
//print_r($idbase);
//print_r($rootNode->description);
if (KnSettings::ARTICLES_CatDesc){
    echo $rootNode->description;
}
?>
<?php if ($grouped) : ?>
    <?php foreach ($list as $groupName => $items) : ?>
    <ul class="mod-articlescategory category-module mod-list">
		<li>
			<div class="mod-articles-category-group"><?php echo Text::_($groupName); ?></div>
			<ul>
				<?php require ModuleHelper::getLayoutPath('mod_articles_category', $params->get('layout', 'default') . '_items'); ?>
			</ul>
		</li>
    </ul>
    <?php endforeach; ?>
<?php else : ?>
    <?php $items = $list; ?>
    <?php require ModuleHelper::getLayoutPath('mod_articles_category', $params->get('layout', 'default') . '_items'); ?>
<?php endif; ?>

