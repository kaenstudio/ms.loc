<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;


if (!$list) {
	return;
}


$i = 0;
?>
<div id="carousel-content-modul" class="carousel carousel-header slide kn-slide" data-ride="carousel" data-interval="5000">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach ($list as $item) : ?>
        <li data-target="#carousel-content-modul" data-slide-to="<?= $i++ ?>" class="<?= $i == 0 ? 'active' : '' ?>"></li>
        <?php endforeach; ?>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php $items = $list; ?>
        <?php require ModuleHelper::getLayoutPath('mod_articles_category', $params->get('layout', 'default') . '_items'); ?>
    </div>
    <!-- Controls -->
    <div class="container my-owl">
        <div class="row">
            <div class="col">
                <a href="#carousel-content-modul" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev"><i class="zmdi zmdi-chevron-left"></i></a>
                <a href="#carousel-content-modul" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next"><i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>