<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   (C) 2020 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
print_r($_COOKIE);
?>


<div class="kn-cookie card" <?php if ($params->get('backgroundimage')) : ?> style="background-image:url(<?php echo $params->get('backgroundimage'); ?>)"<?php endif; ?> >
	<div class="card-body row">
    <div class="cookie-data col-md-10">
		    <?php echo $module->content; ?>
    </div>
    <div class="col-md-2 kn-tc">
      <a href="#" class="btn btn-raised btn-primary">Button</a>
    </div>
	</div>
</div>
<form action=""></form>
