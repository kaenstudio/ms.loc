<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @link: https://codepen.io/dirkpeter/pen/Mqazbo
 * @task: MS-140
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;
?>
<fab>
  <button type="button" title="Actions Menu"></button>
  <ul>
	  <?= $module->content ?>
  </ul>
</fab>