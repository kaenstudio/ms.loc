<?php
/**
 * @package     MS
 * @subpackage  mod_custom
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;
?>


<div itemscope itemtype="https://schema.org/Person" <?php if ($params->get('backgroundimage')) : ?> style="background-image:url(<?php echo $params->get('backgroundimage'); ?>)"<?php endif; ?> >
		<?= $module->content; ?>
</div>