<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @link: https://codepen.io/jo_Geek/pen/gyrZWW
 * @task: MS-138
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;
?>
<div class="fab-wrapper">
	<input id="fabCheckbox" type="checkbox" class="fab-checkbox" />
  <?= $module->content ?>
	<label class="fab" for="fabCheckbox">
		<span class="fab-dots fab-dots-1"></span>
		<span class="fab-dots fab-dots-2"></span>
		<span class="fab-dots fab-dots-3"></span>
	</label>
	<div class="fab-wheel">
		<a class="fab-action fab-action-1">
			<i class="fas fa-question"></i>
		</a>
		<a class="fab-action fab-action-2">
			<i class="fas fa-book"></i>
		</a>
		<a class="fab-action fab-action-3">
			<i class="fas fa-address-book"></i>
		</a>
		<a class="fab-action fab-action-4">
			<i class="fas fa-info"></i>
		</a>
	</div>
</div>
