<?php
/**
 * @package     mod_menu
 * @subpackage  kn-main-menu
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;

/** @var Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $app->getDocument()->getWebAssetManager();

// Подключаем настройки
require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/settings.php');
$knS = new KnSettings();

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}

?>
<ul<?php echo $id; ?> class="navbar-nav <?php echo $class_sfx; ?>">
<?php

// Формируем меню
foreach ($list as $i => &$item)
{
    // Создаем название класса с ID пункта меню
	$itemParams = $item->getParams();
	$knDefault = 0;

	// Отображенни классов для пунктов меню
	if ($knS::MOD_MENU_ItemClass)
    {
	    $class = 'item-' . $item->id;
    }
	else
    {
        $class = '';
    }

	// Пункт меню отмеченный как главная
	if ($item->id == $default_id)
	{
		$class .= ' default';
		$knDefault = 1;
	}

	// Активный пункт меню
	if ($item->id == $active_id || ($item->type === 'alias' && $itemParams->get('aliasoptions') == $active_id))
	{
		$class .= ' current';
	}

	// Создаем CSS Для активного меню
	if (in_array($item->id, $path))
	{
		$class .= ' active';
	}
	elseif ($item->type === 'alias')
	{
		$aliasToId = $itemParams->get('aliasoptions');

		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{
			$class .= ' alias-parent-active';
		}
	}

	// Формируем меню
    require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/normal/normal');
}
?></ul>