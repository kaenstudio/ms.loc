<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;


// Создаем класс если Это разделитель
if ($item->type === 'separator')
{
	$class .= ' dropdown-divider';
}

// Формируем HTML структуру меню

// Структура если есть вложенные меню
if ($item->parent)
{
	if ($item->level === 1) // Если первый уровень то добавляем класс
	{
		echo '<li class="'.'nav-item dropdown '.$class.'">';
	}
	elseif ($item->level === 2)
	{
		echo '<li class="'.'nav-item '.$class.'">';
		// Выбираем действие по типу меню
		switch ($item->type) :
			case 'separator':
			case 'component':
			case 'heading':
			case 'url':
				//require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/tab_menu/' . $item->type);
				echo '<a href="">Test</a>';
				break;

			default:
				//require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/normal/tab_menu/url');
				break;
		endswitch;
		echo '</li>';
	}
//	else
//	{
//		echo '<li class="'.'dropdown-submenu '.$class.'">';
//	}
}
else
{
	// Если это верхний уровень меню
	if ($item->level === 1)
	{
		$class  = 'nav-item'.$class;
	}
	// Проверяем содержит ли клас что-то
//	if(!empty($class))
//	{
//		echo '<li class="'.$class.'">';
//	}
//	else
//	{
//		echo '<li>';
//	}
}



echo $item->id.'/'.$item->level_diff.'/'.$item->level.'/'.$item->shallower.'/'.$item->deeper;
// The next item is deeper.
// Если первый уровень имеет вложенное меню
if ($item->deeper && $item->level === 1)
{
	echo '<ul class="dropdown-menu">';
	echo '<li class="ms-tab-menu">';
	echo '<!-- Nav tabs -->';
	echo '<ul class="nav nav-tabs ms-tab-menu-left" role="tablist">';
//	echo '<li class="' . $class . '">';
//	if ($item->type == 'component')
//	{
//		//require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/normal/tab_menu/component_kn');
//		echo '<a href="">Test!</a>';
//	}
}
// Если второй уроверь имеет вложенное меню
elseif ($item->deeper && $item->level === 2)
{
	echo '<li class="' . $class . '">';
	echo '<a href="">Test2</a>';
	echo '</li>';
}
//// Код по умолчанию
//elseif ($item->deeper)
//{
//	echo '<ul class="dropdown-menu">';
//	$class .= ' deeper';
//	echo '<li class="' . $class . '">';
//}
//// The next item is shallower.
if ($item->shallower && $item->level === 2)
{
	echo '</ul>';
	echo '</li>';
	echo '</ul>';

}
//elseif ($item->shallower)
//{
//	echo '</li>';
//	echo str_repeat('</ul></li>', $item->level_diff);
//}
//// The next item is on the same level.
//else
//{
//	echo '</li>';
//}
if ($item->deeper && $item->level === 1)
{
	echo '<!-- Tab panes -->';
	echo '<div class="tab-content ms-tab-menu-right">';
	echo '<div class="tab-pane active" id="tab-general" role="tabpanel">';
	echo '<a href="">Test3</a>';
}
elseif ($item->shallower && $item->level === 3)
{
	echo '</div>';
	echo '</div>';
}