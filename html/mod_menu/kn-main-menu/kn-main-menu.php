<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;
use Joomla\Registry\Registry;

/** @var Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $app->getDocument()->getWebAssetManager();

// Подключаем настройки
require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/settings.php');
$knS = new KnSettings();

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}

?>
<ul<?php echo $id; ?> class="navbar-nav <?php echo $class_sfx; ?>">
<?php

// Подготовка массива специальных меню
$knMenuArrC = count($list);
$knMenuArr = [];
for ($i=0; $i<$knMenuArrC; $i++)
{
	if ($list[$i]->note == 'tabMenu'){
        $knMenuArr['tab'][] = $list[$i]->route;
    }elseif ($list[$i]->note == 'megaMenu'){
		$knMenuArr['mega'][] = $list[$i]->route;
    }
	else{
		$knMenuArr['tab2'][] = '';
    }
}

// Формируем меню
foreach ($list as $i => &$item)
{
    // Создаем название класса с ID пункта меню
	$itemParams = $item->getParams();

	// Отображенни классов для пунктов меню
	if ($knS->mod_menuItemClass)
    {
	    $class = 'item-' . $item->id;
    }
	else
    {
        $class = '';
    }

	// Пункт меню отмеченный как главная
	if ($item->id == $default_id)
	{
		$class .= ' default';
	}

	// Активный пункт меню
	if ($item->id == $active_id || ($item->type === 'alias' && $itemParams->get('aliasoptions') == $active_id))
	{
		$class .= ' current';
	}

	// Создаем CSS Для активного меню
	if (in_array($item->id, $path))
	{
		$class .= ' active';
	}
	elseif ($item->type === 'alias')
	{
		$aliasToId = $itemParams->get('aliasoptions');

		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{
			$class .= ' alias-parent-active';
		}
	}

	// Обрезаем путь что бы сверить с массивом $knMenuArr
	$knMenuRoute = stristr($item->route, '/', true);

	// Если путь сушествует в ключе tab
	if ((array_key_exists('tab', $knMenuArr) && in_array($knMenuRoute, $knMenuArr['tab'])) || (array_key_exists('tab', $knMenuArr) && in_array($item->route, $knMenuArr['tab'])))
	{
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/tab_menu/tab_menu');
		//echo 'tab';
	}
	// Если путь сушествует в ключе mega
	elseif(array_key_exists('mega', $knMenuArr) && in_array($knMenuRoute, $knMenuArr['mega']))
    {
        require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/mega_menu/mega_menu');
	}
	else
	{
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/normal/normal');
	}
}
?></ul>