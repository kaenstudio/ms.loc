<?php
/**
 * @package     mod_menu
 * @subpackage  kn-main-menu/normal
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */


defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;

$title      = $item->anchor_title ? ' title="' . $item->anchor_title . '"' : '';
$anchor_css = $item->anchor_css ?: '';
$linktype   = $item->title;

if ($item->menu_image)
{
	if ($item->menu_image_css)
	{
		$image_attributes['class'] = $item->menu_image_css;
		$linktype = HTMLHelper::_('image', $item->menu_image, $item->title, $image_attributes);
	}
	else
	{
		$linktype = HTMLHelper::_('image', $item->menu_image, $item->title);
	}

	if ($itemParams->get('menu_text', 1))
	{
		$linktype .= '<span class="image-title">' . $item->title . '</span>';
	}
}

// Добовляем клас ко всему что первого уровня
if ($item->level === 1){
    $knLevel1 = 'nav-link ';

}
elseif ($item->level > 1) // Добовляемя для всего что ниже первого уровня
{
	$knLevel1 = 'dropdown-item ';
}
else
{
	$knLevel1 = '';
}

// Классы для ссылок в меню
// Для создание 3го уровня списка

if ($item->parent && $item->level == 2) // Стили для второго уровня
{
	$knLinkCss = 'dropdown-item has_children';
	if ($item->anchor_css)
	{
		$knLinkCss .= ' '.$item->anchor_css;
	}
}
elseif ($item->parent)
{
	// Если пункт меню родитель то добавлем впереди классы CSS выподающего меню
	$knLinkCss = 'nav-link dropdown-toggle';
}
// Добовляемя для всего что ниже первого уровня
elseif ($item->level > 1)
{
	$knLinkCss = 'dropdown-item';
}
else
{
	$knLinkCss = '';
}

if ($knS::MOD_MENU_Anchor_css && $item->anchor_css)
{
	$knLinkCss .= $item->anchor_css;
}
// Выводим необходимый элемент
if ($item->parent && $item->level == 1)
{
?>
    <a class="<?php echo $knLinkCss ?>" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="<?php echo $item->alias; ?>"><?php echo $linktype; ?> <i class="zmdi zmdi-chevron-down"></i></a>
<?php
}
elseif ($item->parent)
{
?>
	<a href="javascript:void(0)" class="<?php echo $knLinkCss ?>"><?php echo $linktype; ?></a>
<?php
}
else
{
    echo "<span class=\"$knLevel1 $anchor_css\"$title>$linktype</span>";
}
?>
