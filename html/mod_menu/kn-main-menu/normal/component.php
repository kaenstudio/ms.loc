<?php
/**
 * @package     mod_menu
 * @subpackage  kn-main-menu/normal
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Filter\OutputFilter;
use Joomla\CMS\HTML\HTMLHelper;

$attributes = array();

if ($item->anchor_title)
{
    $attributes['title'] = $item->anchor_title;
}

// Классы для ссылок в меню
// Для создание 3го уровня списка
if ($item->parent && $item->level == 2)
{
	$attributes['class'] = 'dropdown-item has_children';
}
elseif ($item->parent)
{
	// Если пункт меню родитель то добавлем впереди классы CSS выподающего меню
	$attributes['class'] = 'nav-link dropdown-toggle';
}
// Если уроверь один то добавить класс. Но чего то я не помню как это работает потомучто ссылки без детей не имеют этого класса
elseif ($item->level > 1)
{
	$attributes['class'] = 'dropdown-item';
}

if ($knS::MOD_MENU_Anchor_css && $item->anchor_css)
{
	if (!array_key_exists('class', $attributes))
	{
		$attributes['class'] = '';
	}
	$attributes['class'] .= ' '.$item->anchor_css;
}

if ($item->anchor_rel)
{
	$attributes['rel'] = $item->anchor_rel;
}

if ($item->id == $active_id) // todo Проверить работает ли эта опция
{
	$attributes['aria-current'] = 'location';

	if ($item->current)
	{
		$attributes['aria-current'] = 'page';
	}
}

// Если сыллка евляется корневой и имеет вложенные пункты меню ставим иконку в конце и добавляем атрибуты
if ($item->parent & $item->level === 1)
{
	$linktype = $item->title.' <i class="zmdi zmdi-chevron-down"></i>';

	// Аттрибуты для ссылки
    $attributes['data-toggle'] = 'dropdown'; // Кликабельное выпадающие меню
    $attributes['data-hover'] = 'dropdown';
    $attributes['role'] = 'button';
    $attributes['aria-haspopup'] = 'true';
    $attributes['aria-expanded'] = 'false';
    $attributes['data-name'] = $item->alias;
}
else
{
	$linktype = $item->title;
}

// если нет картинки но есть CSS создаем из него иконку
if (!$item->menu_image && $item->menu_image_css)
{
	if ($item->level === 1) // Если это первый пункт меню
	{
		$linktype = '<span class="'.$item->menu_image_css.'"></span>&nbsp'.$linktype;
	}
	else // если все остальные
	{
		$linktype = '<i class="'.$item->menu_image_css.'"></i> '.$linktype;
	}

}

if ($item->menu_image)
{
	if ($item->menu_image_css)
	{
		$image_attributes['class'] = $item->menu_image_css;
		$linktype = HTMLHelper::_('image', $item->menu_image, $item->title, $image_attributes);
	}
	else
	{
		$linktype = HTMLHelper::_('image', $item->menu_image, $item->title);
	}

	if ($itemParams->get('menu_text', 1))
	{
		$linktype .= '<span class="image-title">' . $item->title . '</span>';
	}
}

if ($item->browserNav == 1)
{
	$attributes['target'] = '_blank';
}
elseif ($item->browserNav == 2)
{
	$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes';

	$attributes['onclick'] = "window.open(this.href, 'targetWindow', '" . $options . "'); return false;";
}

echo HTMLHelper::_('link', OutputFilter::ampReplace(htmlspecialchars($item->flink, ENT_COMPAT, 'UTF-8', false)), $linktype, $attributes);