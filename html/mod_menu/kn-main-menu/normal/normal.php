<?php
/**
 * @package     mod_menu
 * @subpackage  kn-main-menu/normal
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;

// Создаем класс если Это разделитель
if ($item->type === 'separator')
{
	$class .= ' dropdown-divider';
}

// Создаем HTML структуру меню
if ($item->parent)
{
	if ($item->level === 1) // Если первый уровень то добавляем класс
	{
		echo '<li class="' . 'nav-item dropdown '.$class . '">';
	}
	else
	{
		echo '<li class="' . 'dropdown-submenu '.$class . '">';
	}
}
else
{
	// Если это верхний уровень меню
	if ($item->level === 1)
	{
		$class  = 'nav-item '.$class;
	}
	// Проверяем содержит ли клас что-то
	if(!empty($class))
	{
		echo '<li class="'.$class.'">';
	}
	else
	{
		echo '<li>';
	}
}

// Выбираем действие по типу меню
switch ($item->type) :
	case 'separator':
	case 'component':
	case 'heading':
	case 'url':
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/normal/' . $item->type);
		break;

	default:
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/normal/url');
		break;
endswitch;

// The next item is deeper.
// Если первый уровень имеет вложенное меню
if ($item->deeper && $item->level === 1)
{
	echo '<ul class="dropdown-menu">';
	echo '<li class="'.$class.'">';

	// Если это компонент или ссылка то повторяем ссылку что бы она появилась во всплывающем списке
	if (($item->type == 'component' || $item->type == 'url') && !($knS::MOD_MENU_FirstItem && $knDefault))
	{
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/mega_menu/component_kn');
	}
}
// Если второй уроверь имеет вложенное меню
elseif ($item->deeper && $item->level === 2)
{
	echo '<ul class="dropdown-menu dropdown-menu-left">';
	echo '<li class="'.$class.'">';

	// Если это компонент или ссылка то повторяем ссылку что бы она появилась во всплывающем списке
	if ($item->type == 'component' || $item->type == 'url')
	{
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/mega_menu/component_kn');
	}
}
// Код по умолчанию
elseif ($item->deeper)
{
	echo '<ul class="dropdown-menu">';
	$class .= ' deeper';
	echo '<li class="'.$class.'">';
}
// The next item is shallower.
elseif ($item->shallower)
{
	echo '</li>';
	echo str_repeat('</ul></li>', $item->level_diff);
}
// The next item is on the same level.
else
{
	echo '</li>';
}