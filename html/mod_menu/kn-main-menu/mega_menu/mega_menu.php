<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu/mega_menu
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;

// Создаем класс если Это разделитель
if ($item->type === 'separator')
{
	$class .= ' divider';
}

// Структура если есть вложенные меню
if ($item->parent)
{
	if ($item->level === 1) // Если первый уровень то добавляем класс
	{
		if ($item->note === 'MegaMenu'){ // Если это мега меню
			echo '<li class="' . 'nav-item dropdown dropdown-megamenu-container '.$class . '">';
		}
		else
		{
			echo '<li class="' . 'nav-item dropdown '.$class . '">';
		}
	}
	else // если есть меню 3го уровня
	{
		echo '<li class="' . 'dropdown-submenu '.$class . '">';
	}
}
else
{
	// Если это верхний уровень меню
	if ($item->level === 1)
	{
		$class  = 'nav-item '.$class;
	}
	echo '<li class="' . $class . '">';
}

// Выбираем действие по типу меню
switch ($item->type) :
	case 'separator':
	case 'component':
	case 'heading':
	case 'url':
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/mega_menu/' . $item->type);
		break;

	default:
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/mega_menu/url');
		break;
endswitch;

// The next item is deeper.
// Если первый уровень имеет вложенное меню
if ($item->deeper && $item->level === 1)
{
	echo '<ul class="dropdown-menu">';
	echo '<li class="' . $class . '">';

	// Если это компонент то повторяем ссылку что бы  она появилась во всплывающем списке
	if ($item->type == 'component')
	{
		require ModuleHelper::getLayoutPath('mod_menu', 'kn-main-menu/mega_menu/component_kn');
	}
}
// Если второй уроверь имеет вложенное меню
elseif ($item->deeper && $item->level === 2)
{
	echo '<ul class="dropdown-menu dropdown-menu-left">';
	$class .= '';
	echo '<li class="' . $class . '">';
}
// Код по умолчанию
elseif ($item->deeper)
{
	echo '<ul class="dropdown-menu">';
	$class .= ' deeper';
	echo '<li class="' . $class . '">';
}
// The next item is shallower.
elseif ($item->shallower)
{
	echo '</li>';
	echo str_repeat('</ul></li>', $item->level_diff);
}
// The next item is on the same level.
else
{
	echo '</li>';
}