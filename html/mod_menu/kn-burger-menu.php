<?php
/**
 * @package     mod_menu
 * @subpackage  kn-burger-menu
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;

/** @var Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $app->getDocument()->getWebAssetManager();

// Подключаем настройки
require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/settings.php');
$knS = new KnSettings();

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}
$knMenuType = $list[0]->menutype;
?>

<ul<?php echo $id; ?> class="ms-slidebar-menu <?php echo $class_sfx; ?>" id="<?php echo $knMenuType ?>" role="tablist" aria-multiselectable="true">
<?php foreach ($list as $i => &$item)
{
	$itemParams = $item->getParams();
	$knDefault = 0;

	// Отображенни классов с id для пунктов меню
	if ($knS::BURGER_MENU_ItemClass)
	{
		$class = 'item-' . $item->id;
	}
	else
	{
		$class = '';
	}

	if ($item->id == $default_id)
	{
		$class .= ' default';
		$knDefault = 1;
	}

	if ($item->id == $active_id || ($item->type === 'alias' && $itemParams->get('aliasoptions') == $active_id))
	{
		$class .= ' current';
	}

	if (in_array($item->id, $path))
	{
		$class .= ' active';
	}
	elseif ($item->type === 'alias')
	{
		$aliasToId = $itemParams->get('aliasoptions');

		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{
			$class .= ' alias-parent-active';
		}
	}

	// Класс для разделителя
	if ($item->type === 'separator')
	{
		$class .= 'kn-burger-separator dropdown-divider';
	}

	// Структура если есть вложенные меню
	if ($item->parent && $item->level === 1)
	{

            echo '<li class="' . 'card '.$class . '" role="tab" id="sch'.$item->id.'">';

	        $linktype = $item->title;


            // если нет картинки но есть CSS создаем из него иконку
			if (!$item->menu_image && $item->menu_image_css)
			{
				$linktype = '<i class="'.$item->menu_image_css.'"></i> '.$item->title;
			}


            echo '<a class="collapsed" role="button" data-toggle="collapse" href="#sc'.$item->id.'" aria-expanded="false" aria-controls="sc'.$item->id.'">'.$linktype.' </a>';

	}
	else
    {
        echo '<li class="' . $class . '">';
		switch ($item->type) :
			case 'separator':
			case 'component':
			case 'heading':
			case 'url':
				require ModuleHelper::getLayoutPath('mod_menu', 'kn-burger-menu/kn-burger-menu_' . $item->type);
				break;

			default:
				require ModuleHelper::getLayoutPath('mod_menu', 'kn-burger-menu/kn-burger-menu_url');
				break;
		endswitch;
    }

	// The next item is deeper.
	// Если первый уровень имеет вложенное меню
    if ($item->deeper) // $item->parent && $item->level === 1
    {
	    echo '<ul id="sc'.$item->id.'" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch'.$item->id.'" data-parent="#'.$knMenuType.'">';
	    echo '<li class="' . $class . '">';
	    if (($item->type === 'component' || $item->type === 'url') && !($knS::BURGER_MENU_FirstItem && $knDefault))
        {
		    require ModuleHelper::getLayoutPath('mod_menu', 'kn-burger-menu/kn-burger-menu_' . $item->type);
        }
    }
	// The next item is shallower.
	elseif ($item->shallower)
	{
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	else
	{
		echo '</li>';
	}
}
?></ul>