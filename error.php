<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.ms
 *
 * @copyright   (C) 2022 <https://kaenstudio.com>
 * @license     KaenStudio
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;

/** @var Joomla\CMS\Document\ErrorDocument $this */

$app = Factory::getApplication();
$wa  = $this->getWebAssetManager();

// Подключаем настройки
require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/settings.php');
$knS = new KnSettings();

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');
$menu     = $app->getMenu()->getActive();
$pageclass = $menu !== null ? $menu->getParams()->get('pageclass_sfx', '') : '';

// Template path
$templatePath = 'templates/' . $this->template;

// Color Theme todo переделать по мой шаблон
$paramsColorName = $this->params->get('colorName', 'colors_standard');
$assetColorName  = 'theme.' . $paramsColorName;
$wa->registerAndUseStyle($assetColorName, $templatePath . '/css/global/' . $paramsColorName . '.css');

// Enable assets
$wa->useStyle('template.active.language')
	->useStyle('template.user')
	->useScript('template.user');

// Override 'template.active' asset to set template.ms-css dependency
$wa->registerStyle('template.active', '', [], [], ['template.ms-css']);

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . htmlspecialchars(Uri::root() . $this->params->get('logoFile'), ENT_QUOTES, 'UTF-8') . '" alt="' . $sitename . '">';
}
elseif ($this->params->get('siteTitle'))
{
	$siteTitle = $this->params->get('siteTitle');
	$siteDescription = '';
	if ($this->params->get('siteDescription')){
		$siteDescription = '<strong>'.$this->params->get('siteDescription').'</strong>';
	}
	$logo = '<span class="ms-title" title="' . $sitename . '">' .$siteTitle.$siteDescription . '</span>';
	// Отображение кружка в начале
	if ($knS::MAIN_MENU_LogoCircle)
	{
		$logo = '<span class="ms-logo ms-logo-sm">'.mb_substr($siteTitle, 0, 1).'</span>'.$logo;
	}
}
else
{
	$logo = '';
}


// Container
//$wrapper = $this->params->get('fluidContainer') ? 'wrapper-fluid' : 'wrapper-static';

//$this->setMetaData('viewport', 'width=device-width, initial-scale=1');

// Defer font awesome
//$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');

// Head
//require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/html/layouts/ms/head.php');

// Body classes
if ($knS::JBODY_classes)
{
	$knBodyClasses = 'class="site error_site '.$option.' '.' view-'.$view.($layout ? ' layout-'.$layout : '').($task ? ' task-'.$task : '').($itemid ? ' itemid-'.$itemid : '').($pageclass ? ' '.$pageclass : '').$hasClass.'"';
}

// JGenerator
if ($knS::JGENERATOR === 1)
{
	$wa  = $this->setGenerator(null);
}
elseif ($knS::JGENERATOR === 2)
{
	$wa  = $this->setGenerator('KaenStudio');
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="ltr">
<head>
	<jdoc:include type="metas" />
	<jdoc:include type="styles" />
	<jdoc:include type="scripts" />
</head>
<body <?php echo !empty($knBodyClasses) ? $knBodyClasses : '' ?>>
<?php
if ($knS::ERROR_500 === 1)
{
	require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/html/layouts/page-500_2.php');
}
else
{
	require_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/html/layouts/page-500.php');
}
?>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>